export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  server: {
    port: 4000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  head: {
    title: 'Firebrand 🔥',
    meta: [
      { charset: "utf-8" },
      { name: "viewport",            content: "width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0, height=device-height, target-densitydpi=device-dpi" },
      { name: "url",                 hid: "url",                 content: "https://firebrand.dev"},
      { name: "description",         hid: "description",         content: "🔥"},
      { name: "keywords",            hid: "keywords",            content: "FIREBRAND, FIREBRAND DEV, FIREBRANDDEV GAMEDEV DICTIONARY, FIREBRAND LLC"},
      { name: "image",               hid: "image",               content: "https://firebrand.dev/firebrand-cover.png"},
      { name: "og:site_name",        hid: "og:site_name",        content: "Firebrand 🔥"},
      { name: "og:title",            hid: "og:title",            content: "Firebrand 🔥"},
      { name: "og:url",              hid: "og:url",              content: "https://firebrand.dev"},
      { name: "og:image",            hid: "og:image",            content: "https://firebrand.dev/firebrand-cover.png"},
      { name: "twitter:card",        hid: "twitter:card",        content: "summary"},
      { name: "twitter:url",         hid: "twitter:url",         content: "https://firebrand.dev"},
      { name: "twitter:title",       hid: "twitter:title",       content: "Firebrand 🔥"},
      { name: "twitter:description", hid: "twitter:description", content: "🔥"},
      { name: "twitter:image",       hid: "twitter:image",       content: "https://firebrand.dev/firebrand-cover.png"},
      { 
        name: "og:apple-mobile-web-app-title",        
        hid: "og:apple-mobile-web-app-title",
        content: "Firebrand 🔥"
      },

    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/firebrand-icon32.png' }]
  },
/*
   ** Robots
   */
  robots: {
    UserAgent: '*',
    Disallow: [],
    Sitemap: 'https://firebrand.dev/sitemap.xml'
  },
  /*
   ** Sitemap
   */
  sitemap: {
    hostname: 'https://firebrand.dev',
    path: '/sitemap.xml',
    gzip: true,
    exclude: [],
    routes: []
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['@/assets/css/reset.css', '@/assets/scss/styles.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    "@nuxtjs/sitemap",
    "@nuxtjs/robots",
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.test = /\.(png|jpe?g|gif|webp)$/
      config.module.rules.push({ test: /\.svg$/, loader: 'vue-svg-loader' })
    }
  }
}
